module.exports = {
  pathPrefix: '/', // Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "portfolio"

  siteTitle: 'Felix Calis', // Navigation and Site Title
  siteTitleAlt: 'Felix Calis - Portfolio', // Alternative Site title for SEO
  siteTitleShort: 'Felix', // short_name for manifest
  siteHeadline: 'Publishing & Creating stunning photos', // Headline for schema.org JSONLD
  siteUrl: 'https://www.felixcalis.com', // Domain of your site. No trailing slash!
  siteLanguage: 'en', // Language Tag on <html> element
  siteLogo: '/logos/logo.png', // Used for SEO and manifest
  siteDescription: 'Dark One-Page portfolio with cards & detailed project views',
  author: 'Vyas G', // Author for schema.org JSONLD

  // siteFBAppID: '123456789', // Facebook App ID - Optional
  userTwitter: '@felix', // Twitter Username
  ogSiteName: 'Felix', // Facebook Site Name
  ogLanguage: 'en_US', // og:language
  googleAnalyticsID: 'UA-47519312-4',

  // Manifest and Progress color
  themeColor: '#3498DB',
  backgroundColor: '#2b2e3c',

  // Your information
  name: 'Felix Calis',
  socialMedia: [
    { 
      url: 'https://www.instagram.com/felixcalis/',
      name: 'Instagram',
    },
    {
      url: 'https://www.facebook.com/felix.calis.7?fref=ts',
      name: 'Facebook',
    },
    {
      url: 'https://www.pinterest.com/felixcalis7/',
      
      name: 'Pinterest',
    },
  ],
}
